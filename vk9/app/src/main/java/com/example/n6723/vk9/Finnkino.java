package com.example.n6723.vk9;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Finnkino {

    ArrayList<MovieTheater> theaters = new ArrayList();
    ArrayList<String> theaternames = new ArrayList();
    String theaterName = null;
    String theaterID = null;


    public void listTheaters(){

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            String urlString = "https://www.finnkino.fi/xml/TheatreAreas/";
            Document doc = builder.parse(urlString);
            NodeList nList = doc.getElementsByTagName("TheatreArea");

            for (int i = 0; i < nList.getLength(); i++){

                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE){

                    Element element = (Element) node;
                    theaterName = (element.getElementsByTagName("Name").item(0).getTextContent());
                    theaterID = (element.getElementsByTagName("ID").item(0).getTextContent());

                    MovieTheater temp = new MovieTheater(theaterName, theaterID);
                    theaters.add(temp);

                }

            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void printTheaters(){
        int i = 1;
        for (MovieTheater t : theaters){
            System.out.print(i + ". Nimi: " + t.getName());
            System.out.print(" ID: " + t.getID()+"\n");
            i++;
        }

    }

    public ArrayList<String> getTheaterList(){

        String n ="";

        for (MovieTheater t : theaters){
            n = t.getName();
            theaternames.add(n);
        }

        return theaternames;
    }


    public static ArrayList<String> showMovies(String id, String currentDate, String earliestStartTime, String lastStartTime){

        String movieTitle = null;
        String movieStarts = null;
        String movieEnds = null;
        String auditorium = null;
        String stringForRecyclerView = null;
        ArrayList<String> listOfMoviesToday = new ArrayList();

        String newUrl = ("http://www.finnkino.fi/xml/Schedule/?area="+id+"+&dt="+currentDate);
        System.out.println(newUrl);
        System.out.println(earliestStartTime);

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = builder.parse(newUrl);
            NodeList nList = doc.getElementsByTagName("Show");

            for (int i = 0; i < nList.getLength(); i++){

                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE){

                    Element element = (Element) node;
                    movieTitle = (element.getElementsByTagName("Title").item(0).getTextContent());
                    movieStarts = (element.getElementsByTagName("dttmShowStart").item(0).getTextContent());
                    movieEnds = (element.getElementsByTagName("dttmShowEnd").item(0).getTextContent());
                    auditorium = (element.getElementsByTagName("TheatreAuditorium").item(0).getTextContent());
                    stringForRecyclerView = (movieTitle+"\nKELLO:"+movieStarts.substring(11, 16)+" - "+movieEnds.substring(11, 16)+" SALI: "+auditorium);

                    //Alla karsitaa listattavista elokuvista ne mitkä ei ala vaaditussa aikaikkunassa
                    int start = Integer.valueOf(movieStarts.substring(11,13)+movieStarts.substring(14,16));
                    int earliest = Integer.valueOf(earliestStartTime.substring(0,2)+earliestStartTime.substring(3,5));
                    int lastest = Integer.valueOf(lastStartTime.substring(0,2)+lastStartTime.substring(3,5));

                    if (earliest <= start && start <= lastest) {
                        listOfMoviesToday.add(stringForRecyclerView);
                    }

                }

            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return listOfMoviesToday;

    }

    public String getTheaterID(String T) {
        String movieID = null;

        for (MovieTheater t : theaters) {
            System.out.println(T);
            if (t.getName().equals(T)) {
                movieID = t.getID();
            }

        }
        return (movieID);

    }

}
