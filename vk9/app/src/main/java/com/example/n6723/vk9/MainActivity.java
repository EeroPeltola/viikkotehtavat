package com.example.n6723.vk9;

import android.content.Context;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    Context context = null;
    Finnkino FinnKino = new Finnkino();
    String selectedTheaterName = null;
    String selectedTheaterID = null;
    EditText textInput;
    EditText textInput2;
    EditText textInput3;
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    String date = sdf.format(new Date());
    String startTime = "00:00";
    String endTime = "23:59";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        createSpinner();
        changeDate();
        changeStartTime();
        changeEndTime();
    }


    public void createSpinner() {
        final Spinner theaterOptions = findViewById(R.id.spinner);
        FinnKino.listTheaters();
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, FinnKino.getTheaterList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        theaterOptions.setAdapter(adapter);

        theaterOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedTheaterName = theaterOptions.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    public void hakuButton(View v) {

        //FinnKino.printTheaters();

        selectedTheaterID = FinnKino.getTheaterID(selectedTheaterName);
        System.out.println("####Päivä: " + date);
        createRecyclerView(Finnkino.showMovies(selectedTheaterID, date, startTime, endTime));

    }

    public void createRecyclerView(ArrayList<String> movies) {

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(this, movies);
        recyclerView.setAdapter(adapter);
    }




    private void changeDate() {

        final TextView changingDate =findViewById(R.id.date);
        changingDate.setText(date);
        textInput = findViewById(R.id.date);
        textInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                date = textInput.getText().toString();
            }
        });

    }



    private void changeStartTime() {

        final TextView changingTime =findViewById(R.id.startTime);
        changingTime.setText("00:00");
        textInput2 = findViewById(R.id.startTime);
        textInput2.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               startTime = textInput2.getText().toString();
            }
        });

    }



    private void changeEndTime() {

        final TextView changingTime =findViewById(R.id.endTime);
        changingTime.setText("23:59");
        textInput3 = findViewById(R.id.endTime);
        textInput3.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                endTime = textInput3.getText().toString();
            }
        });

    }


}
