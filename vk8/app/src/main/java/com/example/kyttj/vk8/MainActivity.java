package com.example.kyttj.vk8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.lang.Double.parseDouble;

public class MainActivity extends AppCompatActivity{

    double moneyAmount = 0;
    double nextCoin = 0;
    List<Double> kokolista = Arrays.asList(0.5, 1.5);

    String text = null;
    String drinkOption = "Pepsi Max";
    String sizeOption = "0.5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         main();
    }

    private void main() {

        final BottleDispenser kone = new BottleDispenser();
        final TextView changingText = findViewById(R.id.text_to_change);
        final TextView changingMoneyAmount = findViewById(R.id.editText2);

        Button addMoneyButton = findViewById(R.id.addMoreMoney);
        Button takeMoneyButton = findViewById(R.id.removeMoney);
        Button buyBottleButton = findViewById(R.id.buyBottle);
        SeekBar seekMoneyBar = findViewById(R.id.seekBar);
        final Spinner drinkOptions = findViewById(R.id.juomavalikko);
        final Spinner sizeOptions = findViewById(R.id.kokovalikko);

        addMoneyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moneyAmount += nextCoin;
                text = ("Rahaa koneessa: "+ String.format("%.2f",moneyAmount)+"€\nValinnan hinta: "+kone.getBottlePrice(drinkOption,sizeOption)+"€");
                changingText.setText(text);
            }
        });

        takeMoneyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                moneyAmount = 0;
                text = ("Rahaa koneessa: "+ String.format("%.2f",moneyAmount)+"€\nValinnan hinta: "+kone.getBottlePrice(drinkOption,sizeOption)+"€");
                changingText.setText(text);
                Toast.makeText(getBaseContext(), "Klink klink, rahat ulos!", Toast.LENGTH_LONG).show();

            }
        });

        buyBottleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                    switch(kone.buyBottle(drinkOption,sizeOption,moneyAmount)){

                    case 0:
                        Toast.makeText(getBaseContext(), (drinkOption + " " + sizeOption + " on loppunut"), Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        Toast.makeText(getBaseContext(), ("KACHUNK, ostit " + drinkOption + " " + sizeOption), Toast.LENGTH_LONG).show();
                        moneyAmount -= kone.getLastSoldBottlePrice();
                        text = ("Rahaa koneessa: "+ String.format("%.2f",moneyAmount)+"€\nValinnan hinta: "+kone.getBottlePrice(drinkOption,sizeOption)+"€");
                        changingText.setText(text);


                        //kirjoitetaan kuitti

                        try{
                            Context context = MainActivity.this;

                            String filename = "receipt.txt";

                            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));

                            String s = ("Kuitti: "+ drinkOption + " " + sizeOption+ " " + kone.getLastSoldBottlePrice() +"€");
                            ows.write(s);
                            ows.close();

                        }catch(IOException e) {
                            Log.e("IOExceoption", "Virhe syöttessä");
                        }finally{
                            System.out.println("Kuitti kirjoitettu");
                        }



                        break;
                    case 2:
                        Toast.makeText(getBaseContext(), ("Lisää rahaa!"), Toast.LENGTH_LONG).show();
                        break;
                    default:
                        break;
                }

            }
        });



        seekMoneyBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                switch (progress){
                    case 1:
                        nextCoin = 0.05;
                        break;
                    case 2:
                        nextCoin = 0.10;
                        break;
                    case 3:
                        nextCoin = 0.20;
                        break;
                    case 4:
                        nextCoin = 0.50;
                        break;
                    case 5:
                        nextCoin = 1;
                        break;
                    case 6:
                        nextCoin = 2;
                        break;
                    case 0:
                        nextCoin = 0;
                        break;
                    default:
                        break;
                }
                changingMoneyAmount.setText("   "+ nextCoin +"€");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //Alla juomavalikon koodit

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.juomalista, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        drinkOptions.setAdapter(adapter);
        drinkOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                drinkOption = drinkOptions.getSelectedItem().toString();
                text = ("Rahaa koneessa: "+ String.format("%.2f",moneyAmount)+"€\nValinnan hinta: "+kone.getBottlePrice(drinkOption,sizeOption)+"€");
                changingText.setText(text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> adapter2 = new ArrayAdapter(this,  android.R.layout.simple_spinner_item,kokolista);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sizeOptions.setAdapter(adapter2);
        sizeOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sizeOption = sizeOptions.getSelectedItem().toString();
                text = ("Rahaa koneessa: "+ String.format("%.2f",moneyAmount)+"€\nValinnan hinta: "+kone.getBottlePrice(drinkOption,sizeOption)+"€");
                changingText.setText(text);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    
}