package com.example.kyttj.vk8;

public class Bottle {

    private final String name;
    private final String manufacturer;
    private final String total_energy;
    private final String size;
    private final String price;

    public Bottle() {

        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = "0.3";
        size = "0.5";
        price = "1.8";
    }

    public Bottle(String n, String m, String t, String s, String p) {
        name = n;
        manufacturer = m;
        total_energy = t;
        size = s;
        price = p;
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getEnergy() {
        return total_energy;
    }

    public String getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }

}
