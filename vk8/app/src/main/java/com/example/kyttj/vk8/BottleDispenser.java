package com.example.kyttj.vk8;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import static java.lang.Double.parseDouble;

public class BottleDispenser {


    private double money;
    private double lastSoldBottlePrice;

    ArrayList<Bottle> pullot = new ArrayList();

    public BottleDispenser() {
        String n, m, e, s, p;

        for (int i = 0; i<4; i++){
            n = "Pepsi Max";
            m = "Pepsi";
            e = "2";
            s = "0.5";
            p = "1.8";
            Bottle temp = new Bottle(n, m, e, s, p);
            pullot.add(temp);
        }

        for (int i = 0; i<1; i++){
            n = "Pepsi Max";
            m = "Pepsi";
            e = "2";
            s = "1.5";
            p = "3.6";
            Bottle temp = new Bottle(n, m, e, s, p);
            pullot.add(temp);
        }

        for (int i = 0; i<1; i++){
            n = "Coca-Cola";
            m = "Coca Company";
            e = "2";
            s = "0.5";
            p = "1.8";
            Bottle temp = new Bottle(n, m, e, s, p);
            pullot.add(temp);
        }

        for (int i = 0; i<1; i++){
            n = "Coca-Cola";
            m = "Coca Company";
            e = "2";
            s = "1.5";
            p = "2.7";
            Bottle temp = new Bottle(n, m, e, s, p);
            pullot.add(temp);
        }

        for (int i = 0; i<2; i++){
            n = "Fanta";
            m = "Hartwalli";
            e = "3";
            s = "0.5";
            p = "1.8";
            Bottle temp = new Bottle(n, m, e, s, p);
            pullot.add(temp);
        }

        for (int i = 0; i<2; i++){
            n = "Fanta";
            m = "Hartwalli";
            e = "3";
            s = "1.5";
            p = "3.2";
            Bottle temp = new Bottle(n, m, e, s, p);
            pullot.add(temp);
        }

        money = 0;
    }


    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public int buyBottle(String drink, String size, double money) {

        int successTest = 0;

        for (Iterator<Bottle> it = pullot.iterator(); it.hasNext();){
            Bottle nextBottle = it.next();
            if (nextBottle.getName().equals(drink) && nextBottle.getSize().equals(size)){//checking if the bottle exists
                successTest = 2;//means that there is a drink but no money

                if(parseDouble(nextBottle.getPrice()) <= money) {//checking if the customer has put enough money

                    lastSoldBottlePrice = parseDouble(nextBottle.getPrice());//needed for later
                    it.remove();
                    successTest = 1;//means successfull purchase

                    break;
                }
            }
        }


        return successTest;
    }


    public void returnMoney() {
        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "+df.format(money)+"€");
        money = 0;

    }

    public void listBottles(){
        int i = 1;
        for (Bottle pu : pullot){
            System.out.println(i + ". Nimi: " + pu.getName());
            System.out.print("\tKoko: " + pu.getSize());
            System.out.println("\tHinta: " + pu.getPrice());
            i++;

        }

    }

    public String getBottlePrice(String drink, String size) {
        String price = "Juoma loppunut:(";

        for (Iterator<Bottle> it = pullot.iterator(); it.hasNext();){
            Bottle nextBottle = it.next();
            if (nextBottle.getName().equals(drink) && nextBottle.getSize().equals(size)){
                price = nextBottle.getPrice();
                break;
            }
        }
        return price;
    }


    public Double getLastSoldBottlePrice(){
        return lastSoldBottlePrice;
    }

}