package com.example.n6723.vk11uusyritys;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    homeFragment homeFragment = new homeFragment();
    changeFontFragment changeFontFragment = new changeFontFragment();
    changeColourFragment changeColourFragment = new changeColourFragment();
    changeRowsFragment changeRowsFragment = new changeRowsFragment();
    changeCapitalsFragment changeCapitalsFragment = new changeCapitalsFragment();
    changeEditsettingFragment changeEditsettingFragment = new changeEditsettingFragment();
    LanguageFragment languageFragment = new LanguageFragment();

    String eText;
    String username = "Guestt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area,
                    homeFragment).commit();
            navigationView.setCheckedItem(R.id.nav_camera);
        }



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, homeFragment).commit();

        } else if (id == R.id.nav_gallery) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, changeFontFragment).commit();

        } else if (id == R.id.nav_slideshow) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, changeColourFragment).commit();

        } else if (id == R.id.nav_manage) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, changeRowsFragment).commit();

        } else if (id == R.id.nav_capitals) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, changeCapitalsFragment).commit();

        } else if (id == R.id.nav_language) {
            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, languageFragment).commit();


        } else if (id == R.id.nav_editsetting) {


            EditText texti =  findViewById(R.id.editText);
            System.out.println(texti.getText().toString());
            eText = texti.getText().toString();

            getSupportFragmentManager().beginTransaction().replace(R.id.screen_area, changeEditsettingFragment).commit();


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void fontsizeLarge(View v) {
        Bundle bundle=new Bundle();
        bundle.putString("size", "30");
        homeFragment.setArguments(bundle);
    }

    public void fontsizeMedium(View v) {
        Bundle bundle=new Bundle();
        bundle.putString("size", "20");
        homeFragment.setArguments(bundle);
    }

    public void fontsizeSmall(View v) {
        Bundle bundle=new Bundle();
        bundle.putString("size", "10");
        homeFragment.setArguments(bundle);
    }

    public void textToRed(View v) {
        Bundle bundle = new Bundle();
        int colour = Color.RED;
        String color = Integer.toString(colour);
        bundle.putString("color", color);
        homeFragment.setArguments(bundle);
    }

    public void textToBlack(View v) {
        Bundle bundle = new Bundle();
        int colour = Color.BLACK;
        String color = Integer.toString(colour);
        bundle.putString("color", color);
        homeFragment.setArguments(bundle);
    }

    public void textToBlue(View v) {
        Bundle bundle = new Bundle();
        int colour = Color.BLUE;
        String color = Integer.toString(colour);
        bundle.putString("color", color);
        homeFragment.setArguments(bundle);
    }


    public void threeRows(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("amountoflines", "3");
        homeFragment.setArguments(bundle);
    }
    public void fiveRows(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("amountoflines", "5");
        homeFragment.setArguments(bundle);
    }
    public void eightRows(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("amountoflines", "10");
        homeFragment.setArguments(bundle);
    }


    public void capitalsTrue(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("capitals", "true");
        homeFragment.setArguments(bundle);
    }


    public void capitalsFalse(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("capitals", "false");
        homeFragment.setArguments(bundle);
    }

    public void editYes(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("editSetting", "0");
        homeFragment.setArguments(bundle);
    }

    public void editNo(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("editSetting", eText);
        homeFragment.setArguments(bundle);
    }

    public void editName(View v) {

        Bundle bundle = new Bundle();
        username = languageFragment.getUsername();
        bundle.putString("Username", username);
        System.out.println("Mainactivityssä oleva username: "+ languageFragment.getUsername());
        homeFragment.setArguments(bundle);
    }




}
