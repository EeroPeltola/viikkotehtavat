package com.example.n6723.vk11uusyritys;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import static java.lang.Boolean.parseBoolean;


public class homeFragment extends Fragment {

    KeyListener listener;
    String originalText = "Muokattava teksti, jossa on yli viisikymmentä merkkiä. En jaksa laskea merkkien määrää. Kai niitä on nyt tarpeeksi.";
    TextView textToEdit = null;
    TextView usernameToEdit = null;
    EditText editText;
    String textEditor ="";
    String username = "Guest";
    int koko = 20;
    int color = Color.BLACK;
    int amountOfLines = 5;
    boolean capitals = false;
    boolean allowEdit = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        try {
            textEditor=getArguments().getString("size");
            koko = Integer.parseInt(textEditor);
        } catch (Exception e) {}




        try {
            textEditor=getArguments().getString("color");
             color = Integer.parseInt(textEditor);
        } catch (Exception e) {}



        try {
            textEditor=getArguments().getString("amountoflines");
            amountOfLines = Integer.parseInt(textEditor);
        } catch (Exception e) {}



        try {
            textEditor=getArguments().getString("capitals");
             capitals = parseBoolean(textEditor);
        } catch (Exception e) {}


        try {
            textEditor=getArguments().getString("editSetting");
            if (!textEditor.equals("0")) {
                originalText = textEditor;
                allowEdit = false;
            }
            else {
                allowEdit = true;
            }

        } catch (Exception e) {}


        try {
            username=getArguments().getString("Username");
            System.out.println("TOIMIIIII");
        } catch (Exception e) {}




        return inflater.inflate(R.layout.fragment_home,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView textToEdit = getActivity().findViewById(R.id.textToEdit);
        TextView usernameToEdit = getActivity().findViewById(R.id.usernameEdit);

        editText = getView().findViewById(R.id.editText);
        listener = editText.getKeyListener();

        textToEdit.setText(originalText);
        textToEdit.setTextSize(koko);
        textToEdit.setTextColor(color);
        textToEdit.setLines(amountOfLines);
        textToEdit.setAllCaps(capitals);
        usernameToEdit.setText("Username: " + username);

        if (allowEdit == false) {
            editText.setKeyListener(null);
        }
        if (allowEdit == true) {
                editText.setKeyListener(listener);
                }

        }


}
