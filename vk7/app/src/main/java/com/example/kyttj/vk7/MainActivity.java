package com.example.kyttj.vk7;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    String text;
    String filename;
    Context context = null;
    EditText textInput;
    EditText tiedostoNimi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textInput = findViewById(R.id.textInput);
        tiedostoNimi = findViewById(R.id.tiedostoNimi);

        changeTextOnce();

        context = MainActivity.this;

        System.out.println("Kansion sijainti" + context.getFilesDir());


    }


    private void changeTextOnce() {

        final TextView changingText = findViewById(R.id.text_to_change);

        textInput = findViewById(R.id.textInput);

        textInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                text = textInput.getText().toString();
                changingText.setText(text);
            }
        });

    }


    public void readFile(View v){

        try{

            filename = tiedostoNimi.getText().toString();
            InputStream ins = context.openFileInput(filename);
            String message = "";
            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";

            while ((s= br.readLine())!= null) {
                message += s;

            }

            final TextView changingText = findViewById(R.id.text_to_change);
            changingText.setText(message);

            ins.close();
        }catch(IOException e){
            Log.e("IOExceoption","Virhe syöttessä");
        }finally{
            System.out.println("Luettu");
        }

    }


    public void writeFile(View v){
        try{
            filename = tiedostoNimi.getText().toString();

            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));

            String s = "";
            System.out.println(text);
            s =text;
            ows.write(s);
            ows.close();

        }catch(IOException e) {
            Log.e("IOExceoption", "Virhe syöttessä");
        }finally{
            System.out.println("Kirjoitettu");
        }

    }




}